package com.uk.rest.webservice.restwebservice;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.uk.rest.webservice.restwebservice.service.AddressService;

@Configuration
public class SpringConfig {
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

}
