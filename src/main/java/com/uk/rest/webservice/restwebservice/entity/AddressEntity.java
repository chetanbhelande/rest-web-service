package com.uk.rest.webservice.restwebservice.entity;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * @author cheta
 *
 */
@Entity
@Table(name="ADDRESS")
public class AddressEntity {

	public AddressEntity(Long id, String postCode) {
		super();
		this.id = id;
		this.postcode = postCode;
	}

	public AddressEntity() {
		// TODO Auto-generated constructor stub
	}

	@GeneratedValue
	@Column(name = "ADDR_ID")
	@Id
	private Long id;

	private String postcode;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id")
	private PeopleEntity people;

	public String getPostCode() {
		return postcode;
	}

	public void setPostCode(String postCode) {
		this.postcode = postCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PeopleEntity getPeople() {
		return people;
	}

	public void setPeople(PeopleEntity people) {
		this.people = people;
	}

}
