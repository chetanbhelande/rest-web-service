package com.uk.rest.webservice.restwebservice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description="All details about the PostCard.")
public class PostCode {

	@ApiModelProperty(notes="Will return type of address.")
	private String type;
	
	@ApiModelProperty(notes="postcode")
	private String postcode;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public PostCode(String type, String postcode) {
		super();
		this.type = type;
		this.postcode = postcode;
	}

	public PostCode() {
		super();
	}


}
