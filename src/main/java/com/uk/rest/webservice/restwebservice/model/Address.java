package com.uk.rest.webservice.restwebservice.model;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "All details about the Address.")
public class Address {

	private String type;

	@Size(min = 2, message = "PostCode should have atleast 2 characters")
	@ApiModelProperty(notes = "PostCode should have atleast 2 characters")
	private String postcode;
	
	public Address(String type, @Size(min = 2, message = "PostCode should have atleast 2 characters") String postCode) {
		super();
		this.type = type;
		this.postcode = postCode;
	}

	public Address() {
		super();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPostCode() {
		return postcode;
	}

	public void setPostCode(String postCode) {
		this.postcode = postCode;
	}
	
	@Override
	public String toString() {
		return "Address [type=" + type + ", postCode=" + postcode + "]";
	}

}
