package com.uk.rest.webservice.restwebservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.uk.rest.webservice.restwebservice.entity.AddressEntity;
import com.uk.rest.webservice.restwebservice.entity.PeopleEntity;
import com.uk.rest.webservice.restwebservice.exception.PeopleNotFoundException;
import com.uk.rest.webservice.restwebservice.model.People;
import com.uk.rest.webservice.restwebservice.model.PostCode;
import com.uk.rest.webservice.restwebservice.service.AddressService;
import com.uk.rest.webservice.restwebservice.service.PeopleService;

@RestController
@RequestMapping(path = "/api/v1", produces = { MediaType.APPLICATION_JSON_VALUE })
public class PeopleController {

	@Autowired
	private PeopleService peopleService;

	@Autowired
	private AddressService addressService;;

	@PutMapping("/people/{id}")
	public ResponseEntity<String> createUser(@Valid @RequestBody People newPeople,
			@PathVariable Long id) {

		Long returnId = peopleService.createUpdatePeople(newPeople, id);

//		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("id");
//		FilterProvider filters = new SimpleFilterProvider().addFilter("IDFilter", filter);
//		MappingJacksonValue mapping = new MappingJacksonValue(people);
//		mapping.setFilters(filters);

		if (returnId == id) {
			return new ResponseEntity<String>("{\"id\" : "+returnId+"}", HttpStatus.OK);
		}
		return new ResponseEntity<String>("{\"id\" : "+returnId+"}", HttpStatus.CREATED);
	}

	@GetMapping("/people/{id}")
	public ResponseEntity<People> retrieveUser(@PathVariable Long id) {
	
		People people = peopleService.findPepole(id);
		
		if(people == null) {
			throw new PeopleNotFoundException("Id "+id+" is not found");
		}
			
		return new ResponseEntity<People>(people, HttpStatus.OK);
	}
	
	@GetMapping("/address/postcodes/{postcode}")
	public ResponseEntity<PostCode> retrieveAddressByPostCode(@PathVariable String postcode) {
	
		PostCode postCode = addressService.findAddressByPostCode(postcode);
		
		return new ResponseEntity<PostCode>(postCode, HttpStatus.OK);
	}

}
