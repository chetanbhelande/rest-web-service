package com.uk.rest.webservice.restwebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.uk.rest.webservice.restwebservice.model.PostCode;

@Service
public class AddressService {

	private static final String URL = "http://v2mmr.mocklab.io/json/";
	
	@Autowired
	RestTemplate restTemplate;

	public PostCode findAddressByPostCode(String postcode) {
		
		
		PostCode postCodeObj = restTemplate.getForObject(URL+postcode, PostCode.class);
		
		return postCodeObj;
	}
	
}

