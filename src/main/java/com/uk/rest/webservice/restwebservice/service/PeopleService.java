package com.uk.rest.webservice.restwebservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uk.rest.webservice.restwebservice.entity.AddressEntity;
import com.uk.rest.webservice.restwebservice.entity.PeopleEntity;
import com.uk.rest.webservice.restwebservice.model.Address;
import com.uk.rest.webservice.restwebservice.model.People;
import com.uk.rest.webservice.restwebservice.repository.PeopleRepository;

@Service
public class PeopleService {
	
	@Autowired
	PeopleRepository peopleRepository;
	
	@Autowired
	AddressService addressService;

	public Long createUpdatePeople(People newPeople, Long id) {
	
		Optional<PeopleEntity> optionEntity = peopleRepository.findById(id);
		
		PeopleEntity peopleEntity = null;		
		
		if(!optionEntity.isPresent()) {
			peopleEntity = new PeopleEntity();
			peopleEntity.setId(id);
		}else {
			peopleEntity = optionEntity.get();
		}
		
		BeanUtils.copyProperties(newPeople, peopleEntity,"id");
		
		List<AddressEntity> lstAdd = new ArrayList<AddressEntity>();
		for(int i = 0; i < newPeople.getAddresses().size(); i++){
			AddressEntity addressEntity = new AddressEntity();
			BeanUtils.copyProperties(newPeople.getAddresses().get(i), addressEntity);
          	lstAdd.add(addressEntity);
        }
		peopleEntity.setAddresses(lstAdd);
		
		peopleEntity = peopleRepository.save(peopleEntity);
		
		return peopleEntity.getId();
	}
	
	
	
	public People findPepole(Long id) {
		
		Optional<PeopleEntity> entity = peopleRepository.findById(id);
		
		People people = null;
		if(entity.isPresent()) {
			
			people = new People();
			BeanUtils.copyProperties(entity.get(), people,"addresses"); 
			addAddressBean(people,entity.get().getAddresses());
		
		}
		return people;
	}

	private void addAddressBean(People people, List<AddressEntity> addresses) {

		List<Address> lstAdd = new ArrayList<Address>();
		for(int i = 0; i < addresses.size(); i++){
			Address address = new Address();
			BeanUtils.copyProperties(addresses.get(i), address);
            address.setType(addressService.findAddressByPostCode(address.getPostCode()).getType());
			lstAdd.add(address);
        }
		people.setAddresses(lstAdd);
	}

}
