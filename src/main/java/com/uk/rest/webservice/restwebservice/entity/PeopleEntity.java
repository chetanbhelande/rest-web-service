package com.uk.rest.webservice.restwebservice.entity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@Entity
@Table(name="PEOPLE")
public class PeopleEntity {


	public PeopleEntity() {
		super();
	}



	@GeneratedValue
	@Id
	private Long id;

	private String name;
	
	@OneToMany(fetch = FetchType.EAGER,mappedBy="people",cascade = CascadeType.ALL)
	private List<AddressEntity> addresses;

	public List<AddressEntity> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<AddressEntity> addresses) {
		this.addresses = addresses;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public PeopleEntity(Long id, String name, List<AddressEntity> addresses) {
		super();
		this.id = id;
		this.name = name;
		this.addresses = addresses;
	}


	
	@Override
	public String toString() {
		return "PeopleEntity [id=" + id + ", name=" + name + "]";
	}
}
