package com.uk.rest.webservice.restwebservice;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uk.rest.webservice.restwebservice.model.PostCode;
import com.uk.rest.webservice.restwebservice.repository.PeopleRepository;
import com.uk.rest.webservice.restwebservice.service.AddressService;
import com.uk.rest.webservice.restwebservice.service.PeopleService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringTestConfig.class)
public class AddressServiceTests {


	@Autowired
	private AddressService addressService;
	
	@Autowired
	private PeopleService peopleService;
	
	@MockBean
	private PeopleRepository mocPeopleRepository;

	@Autowired
	private RestTemplate restTemplate;

	private MockRestServiceServer mockServer;
	private ObjectMapper mapper = new ObjectMapper();

	@Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

	@Test
	public void mockingPostCodeService() throws JsonProcessingException, URISyntaxException {

		PostCode postCodeObj = new PostCode("HOME", "EC23CD");

		mockServer.expect(ExpectedCount.once(), requestTo(new URI("http://v2mmr.mocklab.io/json/EC23CD")))
				.andExpect(method(HttpMethod.GET)).andRespond(withStatus(HttpStatus.OK)
						.contentType(MediaType.APPLICATION_JSON).body(mapper.writeValueAsString(postCodeObj)));

		PostCode pCodeObj = addressService.findAddressByPostCode("EC23CD");
	
		mockServer.verify();
		
	}
	
}
