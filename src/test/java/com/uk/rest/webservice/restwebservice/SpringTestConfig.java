package com.uk.rest.webservice.restwebservice;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.uk.rest.webservice.restwebservice.service.AddressService;
import com.uk.rest.webservice.restwebservice.service.PeopleService;

public class SpringTestConfig {
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
	@Bean
	public AddressService addressService() {
	    return new AddressService();
	}
	
	@Bean
	public TestRestTemplate testRestTemplate() {
	    return new TestRestTemplate();
	}
	
	@Bean
	public PeopleService peopleService() {
	    return new PeopleService();
	}
}

