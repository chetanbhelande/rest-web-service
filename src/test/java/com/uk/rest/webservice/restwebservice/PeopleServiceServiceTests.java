package com.uk.rest.webservice.restwebservice;


import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uk.rest.webservice.restwebservice.entity.AddressEntity;
import com.uk.rest.webservice.restwebservice.entity.PeopleEntity;
import com.uk.rest.webservice.restwebservice.model.People;
import com.uk.rest.webservice.restwebservice.model.PostCode;
import com.uk.rest.webservice.restwebservice.repository.PeopleRepository;
import com.uk.rest.webservice.restwebservice.service.PeopleService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringTestConfig.class)
public class PeopleServiceServiceTests {

	@Autowired
	private PeopleService peopleService;
	
	@MockBean
	private PeopleRepository mocPeopleRepository;
	
	
	@Autowired
	private RestTemplate restTemplate;

	private MockRestServiceServer mockServer;
	private ObjectMapper mapper = new ObjectMapper();

	@Before
    public void init() throws JsonProcessingException, URISyntaxException {
        mockServer = MockRestServiceServer.createServer(restTemplate);
       
        PostCode postCodeObj = new PostCode("HOME", "EC2 3CD");

		mockServer.expect(ExpectedCount.once(), requestTo(new URI("http://v2mmr.mocklab.io/json/EC2%203CD")))
				.andExpect(method(HttpMethod.GET)).andRespond(withStatus(HttpStatus.OK)
						.contentType(MediaType.APPLICATION_JSON).body(mapper.writeValueAsString(postCodeObj)));
		
		List<AddressEntity> addresses= new ArrayList<AddressEntity>();
		AddressEntity aEntity = new AddressEntity(101L, "EC2 3CD");
		addresses.add(aEntity);
		PeopleEntity peopleEntity = new PeopleEntity(101L, "Jack",addresses);
	
		when(mocPeopleRepository.findById(101L)).thenReturn(Optional.of(peopleEntity)); 
      
    }

	@Test
	public void findPepoleTest() {

		People people =  peopleService.findPepole(101L);
	
		mockServer.verify();
		
		Assert.assertEquals("Jack",people.getName());
		Assert.assertEquals("HOME",people.getAddresses().get(0).getType());
		
	}
}
