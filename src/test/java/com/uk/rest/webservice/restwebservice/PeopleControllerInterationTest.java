package com.uk.rest.webservice.restwebservice;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uk.rest.webservice.restwebservice.entity.AddressEntity;
import com.uk.rest.webservice.restwebservice.entity.PeopleEntity;
import com.uk.rest.webservice.restwebservice.model.People;
import com.uk.rest.webservice.restwebservice.model.PostCode;
import com.uk.rest.webservice.restwebservice.repository.PeopleRepository;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestWebServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PeopleControllerInterationTest {

 
	@LocalServerPort
	private int port;

	HttpHeaders headers ;
	
	@Autowired
	TestRestTemplate testRestTemplate;
	
	HttpEntity<String> entity = null;
	
	@Autowired
	private RestTemplate restTemplate;

	private MockRestServiceServer mockServer;
	private ObjectMapper mapper = new ObjectMapper();
	
	@MockBean
	private PeopleRepository mocPeopleRepository;
	
	@Before
    public void init() throws JsonProcessingException, URISyntaxException {
		headers = new HttpHeaders();
		headers.add("Accept", "application/json");
		entity = new HttpEntity<String>(null, headers);
		mockServer = MockRestServiceServer.createServer(restTemplate);
		
		PostCode postCodeObj = new PostCode("TESTHOME", "EC2 3CD");

		mockServer.expect(ExpectedCount.once(), requestTo(new URI("http://v2mmr.mocklab.io/json/EC2%203CD")))
				.andExpect(method(HttpMethod.GET)).andRespond(withStatus(HttpStatus.OK)
						.contentType(MediaType.APPLICATION_JSON).body(mapper.writeValueAsString(postCodeObj)));

		List<AddressEntity> addresses= new ArrayList<AddressEntity>();
		AddressEntity aEntity = new AddressEntity(101L, "EC2 3CD");
		addresses.add(aEntity);
		PeopleEntity peopleEntity = new PeopleEntity(101L, "Jack",addresses);
	
		when(mocPeopleRepository.findById(101L)).thenReturn(Optional.of(peopleEntity)); 
		
		when(mocPeopleRepository.save(any(PeopleEntity.class))).thenReturn(peopleEntity); 
		
    }

	
	@Test
	public void testGetPeople() {
		
		ResponseEntity<People> response = testRestTemplate.exchange(
				createURLWithPort("/api/v1/people/101"),
				HttpMethod.GET, entity, People.class);

		Assert.assertEquals(200, response.getStatusCodeValue());
		
		Assert.assertEquals("Jack",response.getBody().getName());
		Assert.assertEquals("TESTHOME",response.getBody().getAddresses().get(0).getType());
		Assert.assertEquals("EC2 3CD",response.getBody().getAddresses().get(0).getPostCode());
		
	
	}
	
	@Test
	public void testPutPeople() {
		
		String jsonBody =  "{\"name\":\"Jack\",\"addresses\":[{\"postcode\":\"EC2 3CD\"},{\"postcode\":\"EC1 1QR\"}]}"; 
		
		headers.add("Accept", "application/json");
		headers.add("Content-Type", "application/json");
		entity = new HttpEntity<String>(jsonBody, headers);
		
		ResponseEntity<People> response = testRestTemplate.exchange(
				createURLWithPort("/api/v1/people/1"),
				HttpMethod.PUT, entity, People.class);

		Assert.assertEquals(201, response.getStatusCodeValue());
		
		ResponseEntity<People> res = testRestTemplate.exchange(
				createURLWithPort("/api/v1/people/101"),
				HttpMethod.PUT, entity, People.class);
		
		Assert.assertEquals(200, res.getStatusCodeValue());
	
	}
	@Test
	public void testPostCode() {
		
		ResponseEntity<People> response = testRestTemplate.exchange(
				createURLWithPort("/api/v1//people/1012"),
				HttpMethod.GET, entity, People.class);

		Assert.assertEquals(404, response.getStatusCodeValue());
		
	}
	
	private String createURLWithPort(String uri) {
		return "http://localhost:"+port + uri;
	}
}

